extends KinematicBody

const MAX_SPEED = 300
const FLY_SPEED = 2500
const RUN_MULTIPLIER = 2
const JUMP_SPEED = 600
const ACCEL = 10
const DEACCEL = 20

onready var camera = $"Camera"
onready var terrain = $"/root/main/Terrain"

var g = -1700
var vel = Vector3()

var yaw = 0
var target_yaw = 0
var pitch = 0
var target_pitch = 0

var fly = true
var on_floor = false

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _physics_process(delta):
	
	#if _is_pressed("attack"):
		
		#if damage_ray.is_colliding():
		#	var cam_xform = camera.get_global_transform()
		#	var dir = -cam_xform.basis[2].normalized();
		#	terrain.damage_block(damage_ray.get_collision_point() + dir * 0.05, 0)
		
	_move(delta)

func _move(delta):

	var dir = Vector3() # Where does the player intend to walk to
	var cam_xform = camera.get_global_transform()
	
	if (Input.is_action_pressed("move_forward")):
		dir += -cam_xform.basis[2]
	if (Input.is_action_pressed("move_backward")):
		dir += cam_xform.basis[2]
	if (Input.is_action_pressed("move_left")):
		dir += -cam_xform.basis[0]
	if (Input.is_action_pressed("move_right")):
		dir += cam_xform.basis[0]
	
	var control_vel = vel
	if not fly:
		dir.y = 0
		control_vel.y = 0
	else:
		if (Input.is_action_pressed("jump")):
			dir.y += 2

	dir = dir.normalized()
	
	var accel
	if (dir.dot(vel) > 0):
		accel = ACCEL
	else:
		accel = DEACCEL
	
	var target = dir * (MAX_SPEED if not fly else FLY_SPEED)
	if (Input.is_action_pressed("run")):
		target *= RUN_MULTIPLIER
	
	control_vel = control_vel.linear_interpolate(target, accel * delta)
	
	vel.x = control_vel.x
	vel.z = control_vel.z
	if fly:
		vel.y = control_vel.y
	
	if not fly:
		if on_floor:
			vel.y = 0
			if (Input.is_action_pressed("jump")):
				vel.y = JUMP_SPEED
		else:
			vel.y += delta * g
	
	self.move_and_slide(vel * delta, Vector3(0, 1, 0), 0.05)
	on_floor = is_on_floor()

func _input(event):
	
	#if event is InputEventMouseMotion:
		#var rel = event.relative
		#yaw = fmod(yaw - rel.x * 0.005, 2*PI)
		#pitch = max(min(pitch - rel.y * 0.005, PI), -PI/2)
		#camera.rotation.y = yaw
		#camera.rotation.x = pitch
	if event is InputEventKey:
		if Input.is_action_just_pressed("fly"):
			fly = not fly
		if (Input.is_action_just_pressed("quit")):
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
