shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_lambert,specular_schlick_ggx,vertex_lighting, unshaded;

//uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;//hint_albedo;
uniform float sunlight_intensity = 1.0;
//uniform float specular;
//uniform float point_size : hint_range(0,128);
//uniform sampler2D texture_metallic : hint_white;
//uniform vec4 metallic_texture_channel;
//uniform sampler2D texture_roughness : hint_white;
//uniform vec4 roughness_texture_channel;

//uniform vec3 uv1_scale;
//uniform vec3 uv1_offset;
//uniform vec3 uv2_scale;
//uniform vec3 uv2_offset;

void vertex() {
	//UV = UV * uv1_scale.xy + uv1_offset.xy;
	//VERTEX.x += sin(TIME);
}

void fragment() {
	vec2 base_uv = UV;
	vec4 albedo_tex = texture(texture_albedo, base_uv);

	float sun_light = COLOR.a * sunlight_intensity;

	float r = clamp(COLOR.r + sun_light, 0.0, 1.0);
	float g = clamp(COLOR.g + sun_light, 0.0, 1.0);
	float b = clamp(COLOR.b + sun_light, 0.0, 1.0);

	ALBEDO = albedo_tex.rgb * vec3(r, g, b);

	//float r = 1.0 - pow(2.71828, 100.0 * COLOR.r);
	//float g = 1.0 - pow(2.71828, 100.0 * COLOR.g);
	//float b = 1.0 - pow(2.71828, 100.0 * COLOR.b);
	
	//float r = clamp(0.01 * pow(1.05, 760.0 * COLOR.r) - 0.01, 0.0, 1.0);
	//float g = clamp(0.01 * pow(1.05, 760.0 * COLOR.g) - 0.01, 0.0, 1.0);
	//float b = clamp(0.01 * pow(1.05, 760.0 * COLOR.b) - 0.01, 0.0, 1.0);

	//ALBEDO = albedo_tex.rgb * vec3(r, g, b);

	METALLIC = 0.0;
	ROUGHNESS = 1.0;
}
