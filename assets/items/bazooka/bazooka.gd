extends ItemNode

var rocket_prefab = load("res://assets/items/bazooka/rocket.tscn")
onready var rocket_pos = $"RocketPos"
onready var camera = $"../.."

var force = 10

func _process(delta):
	
	if (Input.is_action_just_pressed("mouse_left")):
		var rocket = rocket_prefab.instance()
		get_tree().get_root().add_child(rocket)
		
		var cam_xform = camera.get_global_transform()
		
		rocket.dir = -cam_xform.basis[2]
		rocket.translation = rocket_pos.global_transform.origin
		rocket.force = force
	